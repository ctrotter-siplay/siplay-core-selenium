# siplay-core test code lives here!
As a starting point, we want to store all test code for the siplay-core project here - keeps it separate from the source (good idea? bad idea?) so we won't affect new development.  This should be viewed as a pilot/prototype project for the time being.

## Iteration 1

We want to start by adding a smoketest to the staging deployment.  There is a teamcity plugin (already installed) that allows you to call Saucelabs for tests via a Maven task.

Some initial hurdles are:
- Maven is an unknown
- pom.xml - wat
- how to transfer SE-Builder code??

Found this: https://github.com/Ardesco/Selenium-Maven-Template

Have set this repo up as a Maven project.
