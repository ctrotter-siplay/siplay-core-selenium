/**
 * Generated script for New Relic Synthetics
 * Generated using se-builder with New Relic Synthetics Formatter
 *
 * Feel free to explore, or check out the full documentation
 * https://docs.newrelic.com/docs/synthetics/new-relic-synthetics/scripting-monitors/writing-scripted-browsers
 * for details.
 */

/** CONFIGURATIONS **/

// Theshold for duration of entire script - fails test if script lasts longer than X (in ms)
// Default is '0', which means that it won't fail.
var ScriptTimeout = 0;
// Script-wide timeout for all wait and waitAndFind functions (in ms)
var DefaultTimeout = 10000;
// Change to any User Agent you want to use.
// Leave as "default" or empty to use the Synthetics default.
var UserAgent = "default";

/** HELPER VARIABLES AND FUNCTIONS **/

var assert = require('assert'),
  By = $driver.By,
  browser = $browser.manage(),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  prevMsg = '',
  prevStep = 0,
  lastStep = 9999,
  VARS = {};

var log = function(thisStep, thisMsg) {
  if (thisStep > 1 || thisStep == lastStep) {
    var totalTimeElapsed = Date.now() - startTime;
    var prevStepTimeElapsed = totalTimeElapsed - stepStartTime;
    console.log('Step ' + prevStep + ': ' + prevMsg + ' FINISHED. It took ' + prevStepTimeElapsed + 'ms to complete.');
    $util.insights.set('Step ' + prevStep + ': ' + prevMsg, prevStepTimeElapsed);
    if (ScriptTimeout > 0 && totalTimeElapsed > ScriptTimeout) {
      throw new Error('Script timed out. ' + totalTimeElapsed + 'ms is longer than script timeout threshold of ' + ScriptTimeout + 'ms.');
    }
  }
  if (thisStep > 0 && thisStep != lastStep) {
    stepStartTime = Date.now() - startTime;
    console.log('Step ' + thisStep + ': ' + thisMsg + ' STARTED at ' + stepStartTime + 'ms.');
    prevMsg = thisMsg;
    prevStep = thisStep;
  }
};

function isAlertPresent() {
  try {
    var thisAlert = $browser.switchTo().alert();
    return true;
  } catch (err) { return false; }
}

function isElementSelected(el) { return $browser.findElement(el).isSelected(); }

function isTextPresentIn(text, sourceEl) {
  return sourceEl.getText()
  .then(function (wholetext) {
    return wholetext.indexOf(text) != -1;
  });
}

function isTextPresent(text) {
  return isTextPresentIn(text, $browser.findElement(By.tagName('html')));
}

/** BEGINNING OF SCRIPT **/

// Setting User Agent is not then-able, so we do this first (if defined and not default)
if (UserAgent && (0 !== UserAgent.trim().length) && (UserAgent != 'default')) {
  $browser.addHeader('User-Agent', UserAgent);
  console.log('Setting User-Agent to ' + UserAgent);
}

// Get browser capabilities and do nothing with it, so that we start with a then-able command
$browser.getCapabilities().then(function () { })

// Step 1
.then(function() {
  log(1, '$browser.get("https://teamlocker.squadlocker.com/#/lockers/10909")');
  return $browser.get("https://teamlocker.squadlocker.com/#/lockers/10909"); })

// Step 2
.then(function () {
  log(2, 'assertText "DEVPROCESSMONITORGEAR"');
  return $browser.findElement(By.css("h3")); })
  .then(function (el) { return el.getText(); })
  .then(function (text) {
    assert.equal(text, "DEVPROCESSMONITORGEAR", 'assertText "DEVPROCESSMONITORGEAR" FAILED.');
})

// Step 3
.then(function() {
  log(3, 'clickElement "//div[@class=\'locker\']/div/div[2]/div[7]/a/div/figure/div/img"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'locker\']/div/div[2]/div[7]/a/div/figure/div/img"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 4
.then(function() {
  log(4, 'setElementText "personalizationName"');
  return $browser.waitForAndFindElement(By.id("personalizationName"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Mittens"); })

// Step 5
.then(function() {
  log(5, 'clickElement "ul.prices"');
  return $browser.waitForAndFindElement(By.css("ul.prices"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 6
.then(function() {
  log(6, 'setElementText "personalizationNumber"');
  return $browser.waitForAndFindElement(By.id("personalizationNumber"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("55"); })

// Step 7
.then(function() {
  log(7, 'sendKeysToElement "//div[@class=\'quantity\']/input"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'quantity\']/input"), DefaultTimeout); })
.then(function (el) { el.sendKeys("\\undefined"); })

// Step 8
.then(function() {
  log(8, 'clickElement "//div[@class=\'quantity\']/input"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'quantity\']/input"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 9
.then(function() {
  log(9, 'clickElement "//div[@class=\'styles-details\']//button[.=\'Add to Cart ($59.95)\']"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'styles-details\']//button[.=\'Add to Cart ($59.95)\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 10
.then(function() {
  log(10, 'clickElement "//div[@class=\'cart-checkout\']//button[.=\'Checkout\']"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'cart-checkout\']//button[.=\'Checkout\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 11
.then(function() {
  log(11, 'setElementText "input.input"');
  return $browser.waitForAndFindElement(By.css("input.input"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Mittens T. Cat"); })

// Step 12
.then(function() {
  log(12, 'setElementText "//div[@id=\'page-content\']/div/div/div[2]/div[1]/section/input[2]"');
  return $browser.waitForAndFindElement(By.xpath("//div[@id=\'page-content\']/div/div/div[2]/div[1]/section/input[2]"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("14442 "); })

// Step 13
.then(function() {
  log(13, 'sendKeysToElement "//div[@id=\'page-content\']/div/div/div[2]/div[1]/section/input[3]"');
  return $browser.waitForAndFindElement(By.xpath("//div[@id=\'page-content\']/div/div/div[2]/div[1]/section/input[3]"), DefaultTimeout); })
.then(function (el) { el.sendKeys("\\9"); })

// Step 14
.then(function() {
  log(14, 'setElementText "//div[@id=\'page-content\']/div/div/div[2]/div[1]/section/input[2]"');
  return $browser.waitForAndFindElement(By.xpath("//div[@id=\'page-content\']/div/div/div[2]/div[1]/section/input[2]"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("14442 Super Street"); })

// Step 15
.then(function() {
  log(15, 'clickElement "//div[@id=\'page-content\']/div/div/div[2]/div[1]/section/input[3]"');
  return $browser.waitForAndFindElement(By.xpath("//div[@id=\'page-content\']/div/div/div[2]/div[1]/section/input[3]"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 16
.then(function() {
  log(16, 'setElementText "//div[@class=\'city-state-zip\']/div[1]/input"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'city-state-zip\']/div[1]/input"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Los Alamos"); })

// Step 17
.then(function() {
  log(17, 'setElementSelected "//div[@class=\'city-state-zip\']/div[2]/select//option[5]"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'city-state-zip\']/div[2]/select//option[5]"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 18
.then(function() {
  log(18, 'setElementSelected "//div[@class=\'city-state-zip\']/div[2]/select//option[5]"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'city-state-zip\']/div[2]/select//option[5]"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 19
.then(function() {
  log(19, 'setElementText "//div[@class=\'city-state-zip\']/div[3]/input"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'city-state-zip\']/div[3]/input"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("90210"); })

// Step 20
.then(function() {
  log(20, 'clickElement "//div[@id=\'page-content\']//button[.=\'Next\']"');
  return $browser.waitForAndFindElement(By.xpath("//div[@id=\'page-content\']//button[.=\'Next\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 21
.then(function () {
  log(21, 'assertElementPresent "Next"');
  $browser.isElementPresent(By.name("Next")).then(function (bool) {
    assert.ok((bool), 'assertElementPresent FAILED.');
  });
})

.then(function() {
  log(lastStep, '');
  console.log('Browser script execution SUCCEEDED.');
}, function(err) {
  console.log ('Browser script execution FAILED.');
  throw(err);
});

/** END OF SCRIPT **/