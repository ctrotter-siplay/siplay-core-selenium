/**
 * Generated script for New Relic Synthetics
 * Generated using se-builder with New Relic Synthetics Formatter
 *
 * Feel free to explore, or check out the full documentation
 * https://docs.newrelic.com/docs/synthetics/new-relic-synthetics/scripting-monitors/writing-scripted-browsers
 * for details.
 */

/** CONFIGURATIONS **/

// Theshold for duration of entire script - fails test if script lasts longer than X (in ms)
// Default is '0', which means that it won't fail.
var ScriptTimeout = 0;
// Script-wide timeout for all wait and waitAndFind functions (in ms)
var DefaultTimeout = 10000;
// Change to any User Agent you want to use.
// Leave as "default" or empty to use the Synthetics default.
var UserAgent = "default";

/** HELPER VARIABLES AND FUNCTIONS **/

var assert = require('assert'),
  By = $driver.By,
  browser = $browser.manage(),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  prevMsg = '',
  prevStep = 0,
  lastStep = 9999,
  VARS = {};

var log = function(thisStep, thisMsg) {
  if (thisStep > 1 || thisStep == lastStep) {
    var totalTimeElapsed = Date.now() - startTime;
    var prevStepTimeElapsed = totalTimeElapsed - stepStartTime;
    console.log('Step ' + prevStep + ': ' + prevMsg + ' FINISHED. It took ' + prevStepTimeElapsed + 'ms to complete.');
    $util.insights.set('Step ' + prevStep + ': ' + prevMsg, prevStepTimeElapsed);
    if (ScriptTimeout > 0 && totalTimeElapsed > ScriptTimeout) {
      throw new Error('Script timed out. ' + totalTimeElapsed + 'ms is longer than script timeout threshold of ' + ScriptTimeout + 'ms.');
    }
  }
  if (thisStep > 0 && thisStep != lastStep) {
    stepStartTime = Date.now() - startTime;
    console.log('Step ' + thisStep + ': ' + thisMsg + ' STARTED at ' + stepStartTime + 'ms.');
    prevMsg = thisMsg;
    prevStep = thisStep;
  }
};

function isAlertPresent() {
  try {
    var thisAlert = $browser.switchTo().alert();
    return true;
  } catch (err) { return false; }
}

function isElementSelected(el) { return $browser.findElement(el).isSelected(); }

function isTextPresentIn(text, sourceEl) {
  return sourceEl.getText()
  .then(function (wholetext) {
    return wholetext.indexOf(text) != -1;
  });
}

function isTextPresent(text) {
  return isTextPresentIn(text, $browser.findElement(By.tagName('html')));
}

/** BEGINNING OF SCRIPT **/

// Setting User Agent is not then-able, so we do this first (if defined and not default)
if (UserAgent && (0 !== UserAgent.trim().length) && (UserAgent != 'default')) {
  $browser.addHeader('User-Agent', UserAgent);
  console.log('Setting User-Agent to ' + UserAgent);
}

// Get browser capabilities and do nothing with it, so that we start with a then-able command
$browser.getCapabilities().then(function () { })

// Step 1
.then(function() {
  log(1, '$browser.get("https://login.siplay.com/Users/Login?leagueId=12866202")');
  return $browser.get("https://login.siplay.com/Users/Login?leagueId=12866202"); })

// Step 2
.then(function() {
  log(2, 'setElementText "Email"');
  return $browser.waitForAndFindElement(By.id("Email"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("cleanup.account@sip.test.com"); })

// Step 3
.then(function() {
  log(3, 'setElementText "Password"');
  return $browser.waitForAndFindElement(By.id("Password"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("AutomatedTestPassword01"); })

// Step 4
.then(function() {
  log(4, 'clickElement "//form[@id=\'loginForm\']//button[.=\' Login\']"');
  return $browser.waitForAndFindElement(By.xpath("//form[@id=\'loginForm\']//button[.=\' Login\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 5
.then(function() {
  log(5, 'clickElement "Website"');
  return $browser.waitForAndFindElement(By.linkText("Website"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 6
.then(function() {
  log(6, 'clickElement "_admin_website_menu"');
  return $browser.waitForAndFindElement(By.id("_admin_website_menu"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 7
.then(function() {
  log(7, 'clickElement "//aside[@id=\'left-panel\']/nav/ul/li[7]/a/span"');
  return $browser.waitForAndFindElement(By.xpath("//aside[@id=\'left-panel\']/nav/ul/li[7]/a/span"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 8
.then(function() {
  log(8, 'clickElement "_admin_scheduling_view_schedules"');
  return $browser.waitForAndFindElement(By.id("_admin_scheduling_view_schedules"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 9
.then(function() {
  log(9, 'clickElement "input.btn.btn-success"');
  return $browser.waitForAndFindElement(By.css("input.btn.btn-success"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 10
.then(function() {
  log(10, 'clickElement "Registrations"');
  return $browser.waitForAndFindElement(By.linkText("Registrations"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 11
.then(function() {
  log(11, 'clickElement "http___devprocessmonitor_siplay_com_lss_admin_registrationdetail_aspx"');
  return $browser.waitForAndFindElement(By.id("http___devprocessmonitor_siplay_com_lss_admin_registrationdetail_aspx"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 12
.then(function() {
  log(12, 'clickElement "View Report"');
  return $browser.waitForAndFindElement(By.linkText("View Report"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 13
.then(function() {
  log(13, 'clickElement "Accounts"');
  return $browser.waitForAndFindElement(By.linkText("Accounts"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 14
.then(function() {
  log(14, 'clickElement "http___devprocessmonitor_siplay_com_lss_admin_reports_allaccounts_aspx"');
  return $browser.waitForAndFindElement(By.id("http___devprocessmonitor_siplay_com_lss_admin_reports_allaccounts_aspx"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 15
.then(function() {
  log(15, 'clickElement "Teams"');
  return $browser.waitForAndFindElement(By.linkText("Teams"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 16
.then(function() {
  log(16, 'clickElement "http___devprocessmonitor_siplay_com_admin_teams"');
  return $browser.waitForAndFindElement(By.id("http___devprocessmonitor_siplay_com_admin_teams"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 17
.then(function() {
  log(17, 'clickElement "Setup"');
  return $browser.waitForAndFindElement(By.linkText("Setup"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 18
.then(function() {
  log(18, 'clickElement "Organization Settings"');
  return $browser.waitForAndFindElement(By.linkText("Organization Settings"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 19
.then(function() {
  log(19, 'clickElement "http___devprocessmonitor_siplay_com_admin_agreements_manageserviceagreement"');
  return $browser.waitForAndFindElement(By.id("http___devprocessmonitor_siplay_com_admin_agreements_manageserviceagreement"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 20
.then(function() {
  log(20, 'clickElement "http___devprocessmonitor_siplay_com_lss_admin_dashboard_aspx"');
  return $browser.waitForAndFindElement(By.id("http___devprocessmonitor_siplay_com_lss_admin_dashboard_aspx"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 21
.then(function() {
  log(21, 'clickElement "Log Out"');
  return $browser.waitForAndFindElement(By.linkText("Log Out"), DefaultTimeout); })
.then(function (el) { el.click(); })

.then(function() {
  log(lastStep, '');
  console.log('Browser script execution SUCCEEDED.');
}, function(err) {
  console.log ('Browser script execution FAILED.');
  throw(err);
});

/** END OF SCRIPT **/