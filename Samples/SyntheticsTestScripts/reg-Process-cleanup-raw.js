/**
 * Generated script for New Relic Synthetics
 * Generated using se-builder with New Relic Synthetics Formatter
 *
 * Feel free to explore, or check out the full documentation
 * https://docs.newrelic.com/docs/synthetics/new-relic-synthetics/scripting-monitors/writing-scripted-browsers
 * for details.
 */

/** CONFIGURATIONS **/

// Theshold for duration of entire script - fails test if script lasts longer than X (in ms)
// Default is '0', which means that it won't fail.
var ScriptTimeout = 0;
// Script-wide timeout for all wait and waitAndFind functions (in ms)
var DefaultTimeout = 10000;
// Change to any User Agent you want to use.
// Leave as "default" or empty to use the Synthetics default.
var UserAgent = "default";

/** HELPER VARIABLES AND FUNCTIONS **/

var assert = require('assert'),
  By = $driver.By,
  browser = $browser.manage(),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  prevMsg = '',
  prevStep = 0,
  lastStep = 9999,
  VARS = {};

var log = function(thisStep, thisMsg) {
  if (thisStep > 1 || thisStep == lastStep) {
    var totalTimeElapsed = Date.now() - startTime;
    var prevStepTimeElapsed = totalTimeElapsed - stepStartTime;
    console.log('Step ' + prevStep + ': ' + prevMsg + ' FINISHED. It took ' + prevStepTimeElapsed + 'ms to complete.');
    $util.insights.set('Step ' + prevStep + ': ' + prevMsg, prevStepTimeElapsed);
    if (ScriptTimeout > 0 && totalTimeElapsed > ScriptTimeout) {
      throw new Error('Script timed out. ' + totalTimeElapsed + 'ms is longer than script timeout threshold of ' + ScriptTimeout + 'ms.');
    }
  }
  if (thisStep > 0 && thisStep != lastStep) {
    stepStartTime = Date.now() - startTime;
    console.log('Step ' + thisStep + ': ' + thisMsg + ' STARTED at ' + stepStartTime + 'ms.');
    prevMsg = thisMsg;
    prevStep = thisStep;
  }
};

function isAlertPresent() {
  try {
    var thisAlert = $browser.switchTo().alert();
    return true;
  } catch (err) { return false; }
}

function isElementSelected(el) { return $browser.findElement(el).isSelected(); }

function isTextPresentIn(text, sourceEl) {
  return sourceEl.getText()
  .then(function (wholetext) {
    return wholetext.indexOf(text) != -1;
  });
}

function isTextPresent(text) {
  return isTextPresentIn(text, $browser.findElement(By.tagName('html')));
}

/** BEGINNING OF SCRIPT **/

// Setting User Agent is not then-able, so we do this first (if defined and not default)
if (UserAgent && (0 !== UserAgent.trim().length) && (UserAgent != 'default')) {
  $browser.addHeader('User-Agent', UserAgent);
  console.log('Setting User-Agent to ' + UserAgent);
}

// Get browser capabilities and do nothing with it, so that we start with a then-able command
$browser.getCapabilities().then(function () { })

// Step 40
.then(function() {
  log(40, '$browser.get("https://login.siplay.com/Users/CreateUser?associatedLeague=12866202")');
  return $browser.get("https://login.siplay.com/Users/CreateUser?associatedLeague=12866202"); })

// Step 41
.then(function() {
  log(41, 'setElementText "Email"');
  return $browser.waitForAndFindElement(By.id("Email"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("zlawsmkn@tafmail.com"); })

// Step 42
.then(function() {
  log(42, 'setElementText "ConfirmEmail"');
  return $browser.waitForAndFindElement(By.id("ConfirmEmail"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("zlawsmkn@tafmail.com"); })

// Step 43
.then(function() {
  log(43, 'setElementText "Password"');
  return $browser.waitForAndFindElement(By.id("Password"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Password01"); })

// Step 44
.then(function() {
  log(44, 'setElementText "ConfirmPassword"');
  return $browser.waitForAndFindElement(By.id("ConfirmPassword"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Password01"); })

// Step 45
.then(function() {
  log(45, 'clickElement "//div[@class=\'panel-body\']//button[.=\'Create User\']"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'panel-body\']//button[.=\'Create User\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 46
.then(function() {
  log(46, 'setElementText "Email"');
  return $browser.waitForAndFindElement(By.id("Email"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("zlawsmkn@tafmail.com"); })

// Step 47
.then(function() {
  log(47, 'setElementText "Password"');
  return $browser.waitForAndFindElement(By.id("Password"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Password01"); })

// Step 48
.then(function() {
  log(48, 'clickElement "//form[@id=\'loginForm\']//button[.=\' Login\']"');
  return $browser.waitForAndFindElement(By.xpath("//form[@id=\'loginForm\']//button[.=\' Login\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 49
.then(function() {
  log(49, 'setElementText "FirstName"');
  return $browser.waitForAndFindElement(By.id("FirstName"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("FirstName"); })

// Step 50
.then(function() {
  log(50, 'setElementText "LastName"');
  return $browser.waitForAndFindElement(By.id("LastName"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("LastName"); })

// Step 51
.then(function() {
  log(51, 'setElementText "Street"');
  return $browser.waitForAndFindElement(By.id("Street"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("5572 Main St"); })

// Step 52
.then(function() {
  log(52, 'setElementText "City"');
  return $browser.waitForAndFindElement(By.id("City"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Oshawa"); })

// Step 53
.then(function() {
  log(53, 'setElementText "State"');
  return $browser.waitForAndFindElement(By.id("State"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("CA"); })

// Step 54
.then(function() {
  log(54, 'setElementText "Zip"');
  return $browser.waitForAndFindElement(By.id("Zip"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("90210"); })

// Step 55
.then(function() {
  log(55, 'setElementText "Phone"');
  return $browser.waitForAndFindElement(By.id("Phone"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("233-444-5555"); })

// Step 56
.then(function() {
  log(56, 'clickElement "//div[@class=\'panel-footer\']//button[.=\' Create Account\']"');
  return $browser.waitForAndFindElement(By.xpath("//div[@class=\'panel-footer\']//button[.=\' Create Account\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 57
.then(function() {
  log(57, 'clickElement "Skip"');
  return $browser.waitForAndFindElement(By.linkText("Skip"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 58
.then(function() {
  log(58, 'clickElement "//label[@for=\'New_Children\']"');
  return $browser.waitForAndFindElement(By.xpath("//label[@for=\'New_Children\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 59
.then(function() {
  log(59, 'setElementSelected "New_Children"');
  return $browser.waitForAndFindElement(By.id("New_Children"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 60
.then(function() {
  log(60, 'clickElement "button.btn.btn-primary"');
  return $browser.waitForAndFindElement(By.css("button.btn.btn-primary"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 61
.then(function() {
  log(61, 'setElementText "FirstName"');
  return $browser.waitForAndFindElement(By.id("FirstName"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("ChildName"); })

// Step 62
.then(function() {
  log(62, 'sendKeysToElement "LastName"');
  return $browser.waitForAndFindElement(By.id("LastName"), DefaultTimeout); })
.then(function (el) { el.sendKeys("\\9"); })

// Step 63
.then(function() {
  log(63, 'setElementText "Nickname"');
  return $browser.waitForAndFindElement(By.id("Nickname"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Mittens"); })

// Step 64
.then(function() {
  log(64, 'setElementText "Birthdate"');
  return $browser.waitForAndFindElement(By.id("Birthdate"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("01/01/2005"); })

// Step 65
.then(function() {
  log(65, 'setElementSelected "//select[@id=\'Grade\']//option[1]"');
  return $browser.waitForAndFindElement(By.xpath("//select[@id=\'Grade\']//option[1]"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 66
.then(function() {
  log(66, 'setElementSelected "//select[@id=\'Grade\']//option[9]"');
  return $browser.waitForAndFindElement(By.xpath("//select[@id=\'Grade\']//option[9]"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 67
.then(function() {
  log(67, 'setElementSelected "//select[@id=\'Grade\']//option[9]"');
  return $browser.waitForAndFindElement(By.xpath("//select[@id=\'Grade\']//option[9]"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 68
.then(function() {
  log(68, 'setElementSelected "GenderValue_0"');
  return $browser.waitForAndFindElement(By.id("GenderValue_0"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 69
.then(function() {
  log(69, 'clickElement "//label[@for=\'GenderValue_0\']"');
  return $browser.waitForAndFindElement(By.xpath("//label[@for=\'GenderValue_0\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 70
.then(function() {
  log(70, 'setElementSelected "GenderValue_0"');
  return $browser.waitForAndFindElement(By.id("GenderValue_0"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 71
.then(function() {
  log(71, 'setElementText "MedicalInfoValue"');
  return $browser.waitForAndFindElement(By.id("MedicalInfoValue"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("None"); })

// Step 72
.then(function() {
  log(72, 'clickElement "//div[@id=\'contentArea\']//button[.=\'Save\']"');
  return $browser.waitForAndFindElement(By.xpath("//div[@id=\'contentArea\']//button[.=\'Save\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 73
.then(function() {
  log(73, 'clickElement "button.btn.btn-primary"');
  return $browser.waitForAndFindElement(By.css("button.btn.btn-primary"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 74
.then(function() {
  log(74, 'clickElement "button.btn.btn-primary"');
  return $browser.waitForAndFindElement(By.css("button.btn.btn-primary"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 75
.then(function() {
  log(75, 'setElementText "FirstName"');
  return $browser.waitForAndFindElement(By.id("FirstName"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("SpouseName"); })

// Step 76
.then(function() {
  log(76, 'setElementText "LastName"');
  return $browser.waitForAndFindElement(By.id("LastName"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("LastName"); })

// Step 77
.then(function() {
  log(77, 'setElementText "Phone"');
  return $browser.waitForAndFindElement(By.id("Phone"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("234-444-5555"); })

// Step 78
.then(function() {
  log(78, 'clickElement "//div[@id=\'contentArea\']//button[.=\'Save & Continue\']"');
  return $browser.waitForAndFindElement(By.xpath("//div[@id=\'contentArea\']//button[.=\'Save & Continue\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 79
.then(function() {
  log(79, 'clickElement "Continue to Checkout"');
  return $browser.waitForAndFindElement(By.linkText("Continue to Checkout"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 80
.then(function() {
  log(80, 'clickElement "Pay with Check"');
  return $browser.waitForAndFindElement(By.linkText("Pay with Check"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 81
.then(function() {
  log(81, 'clickElement "Logout"');
  return $browser.waitForAndFindElement(By.linkText("Logout"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 82
.then(function() {
  log(82, 'clickElement "Log In"');
  return $browser.waitForAndFindElement(By.linkText("Log In"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 83
.then(function() {
  log(83, 'setElementText "Email"');
  return $browser.waitForAndFindElement(By.id("Email"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("chris.trotter@siplay.com"); })

// Step 84
.then(function() {
  log(84, 'setElementText "Password"');
  return $browser.waitForAndFindElement(By.id("Password"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Rec0gniti0nSIPLAY"); })

// Step 85
.then(function() {
  log(85, 'clickElement "//form[@id=\'loginForm\']//button[.=\' Login\']"');
  return $browser.waitForAndFindElement(By.xpath("//form[@id=\'loginForm\']//button[.=\' Login\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 86
.then(function() {
  log(86, 'clickElement "Registrations"');
  return $browser.waitForAndFindElement(By.linkText("Registrations"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 87
.then(function() {
  log(87, 'clickElement "http___devprocessmonitor_siplay_com_lss_admin_registrationdetail_aspx"');
  return $browser.waitForAndFindElement(By.id("http___devprocessmonitor_siplay_com_lss_admin_registrationdetail_aspx"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 88
.then(function() {
  log(88, 'clickElement "btnView"');
  return $browser.waitForAndFindElement(By.id("btnView"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 89
.then(function() {
  log(89, 'clickElement "Edit Acct"');
  return $browser.waitForAndFindElement(By.linkText("Edit Acct"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 90
.then(function() {
  log(90, 'clickElement "Order History"');
  return $browser.waitForAndFindElement(By.linkText("Order History"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 91
.then(function() {
  log(91, 'clickElement "Edit"');
  return $browser.waitForAndFindElement(By.linkText("Edit"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 92
.then(function() {
  log(92, 'clickElement "Unregister"');
  return $browser.waitForAndFindElement(By.linkText("Unregister"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 93
.then(function() {
  log(93, 'clickElement "btnDone"');
  return $browser.waitForAndFindElement(By.id("btnDone"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 94
.then(function() {
  log(94, 'clickElement "Done"');
  return $browser.waitForAndFindElement(By.linkText("Done"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 95
.then(function() {
  log(95, 'clickElement "Delete Account"');
  return $browser.waitForAndFindElement(By.linkText("Delete Account"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 96
.then(function() {
  log(96, 'clickElement "Log Out"');
  return $browser.waitForAndFindElement(By.linkText("Log Out"), DefaultTimeout); })
.then(function (el) { el.click(); })

.then(function() {
  log(lastStep, '');
  console.log('Browser script execution SUCCEEDED.');
}, function(err) {
  console.log ('Browser script execution FAILED.');
  throw(err);
});

/** END OF SCRIPT **/