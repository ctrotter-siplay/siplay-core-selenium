var wd = require('wd')
  , chai = require('chai')
  , chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
chai.should();
chaiAsPromised.transferPromiseness = wd.transferPromiseness;
var VARS = {};
var b = wd.promiseChainRemote();
b.init({browserName:'firefox'})
.get("https://login.siplay.com/Users/CreateUser?associatedLeague=12866202")
.elementById("Email").clear().type("zlddjirp@6paq.com")
.elementById("ConfirmEmail").clear().type("zlddjirp@6paq.com")
.elementById("Password").clear().type("Password01")
.elementById("ConfirmPassword").clear().type("Password01")
.elementByXPath("//div[@class='panel-body']//button[.='Create User']").click()
.elementById("FirstName").clear().type("FirstName")
.elementById("LastName").clear().type("LastName")
.elementById("Street").clear().type("385462 Union St")
.elementById("City").clear().type("Palo Alto")
.elementById("State").clear().type("CA")
.elementById("Zip").clear().type("90210")
.elementById("Phone").clear().type("345-237-4444")
.elementByXPath("//div[@class='panel-footer']//button[.=' Create Account']").click()
.elementByLinkText("Skip").click()
.elementByXPath("//label[@for='New_Children']").click()
.elementById("New_Children")
.then(function (el) { b.isSelected(el)
  .then(function (isSelected) {
    if (!isSelected) {
      b.clickElement(el);
    }
  });
})
.elementByCssSelector("button.btn.btn-primary").click()
.elementById("FirstName").clear().type("Child1")
.elementById("LastName").type("\\9")
.elementById("Nickname").clear().type("Mittens")
.elementById("Birthdate").clear().type("01/01/2008")
.elementByXPath("//select[@id='Grade']//option[1]")
.then(function (el) { b.isSelected(el)
  .then(function (isSelected) {
    if (!isSelected) {
      b.clickElement(el);
    }
  });
})
.elementByXPath("//select[@id='Grade']//option[1]")
.then(function (el) { b.isSelected(el)
  .then(function (isSelected) {
    if (!isSelected) {
      b.clickElement(el);
    }
  });
})
.elementByXPath("//select[@id='Grade']//option[9]")
.then(function (el) { b.isSelected(el)
  .then(function (isSelected) {
    if (!isSelected) {
      b.clickElement(el);
    }
  });
})
.elementByXPath("//select[@id='Grade']//option[9]")
.then(function (el) { b.isSelected(el)
  .then(function (isSelected) {
    if (!isSelected) {
      b.clickElement(el);
    }
  });
})
.elementByXPath("//label[@for='GenderValue_0']").click()
.elementById("GenderValue_0")
.then(function (el) { b.isSelected(el)
  .then(function (isSelected) {
    if (!isSelected) {
      b.clickElement(el);
    }
  });
})
.elementById("MedicalInfoValue").clear().type("None")
.elementByXPath("//div[@id='contentArea']//button[.='Save']").click()
.elementByCssSelector("button.btn.btn-primary").click()
.elementByCssSelector("button.btn.btn-primary").click()
.elementById("FirstName").clear().type("Spouse")
.elementById("LastName").clear().type("LastName")
.elementById("Phone").clear().type("552-333-6666")
.elementByXPath("//div[@id='contentArea']//button[.='Save & Continue']").click()
.elementByLinkText("Continue to Checkout").click()
.elementByLinkText("Pay with Check").click()
.elementByLinkText("Logout").click()
.fin(function() { return b.quit() })
.done();