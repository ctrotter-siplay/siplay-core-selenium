/**
 * Generated script for New Relic Synthetics
 * Generated using se-builder with New Relic Synthetics Formatter
 *
 * Feel free to explore, or check out the full documentation
 * https://docs.newrelic.com/docs/synthetics/new-relic-synthetics/scripting-monitors/writing-scripted-browsers
 * for details.
 */

/** CONFIGURATIONS **/

// Theshold for duration of entire script - fails test if script lasts longer than X (in ms)
// Default is '0', which means that it won't fail.
var ScriptTimeout = 0;
// Script-wide timeout for all wait and waitAndFind functions (in ms)
var DefaultTimeout = 10000;
// Change to any User Agent you want to use.
// Leave as "default" or empty to use the Synthetics default.
var UserAgent = "default";

/** HELPER VARIABLES AND FUNCTIONS **/

var assert = require('assert'),
  By = $driver.By,
  browser = $browser.manage(),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  prevMsg = '',
  prevStep = 0,
  lastStep = 9999,
  VARS = {};

var log = function(thisStep, thisMsg) {
  if (thisStep > 1 || thisStep == lastStep) {
    var totalTimeElapsed = Date.now() - startTime;
    var prevStepTimeElapsed = totalTimeElapsed - stepStartTime;
    console.log('Step ' + prevStep + ': ' + prevMsg + ' FINISHED. It took ' + prevStepTimeElapsed + 'ms to complete.');
    $util.insights.set('Step ' + prevStep + ': ' + prevMsg, prevStepTimeElapsed);
    if (ScriptTimeout > 0 && totalTimeElapsed > ScriptTimeout) {
      throw new Error('Script timed out. ' + totalTimeElapsed + 'ms is longer than script timeout threshold of ' + ScriptTimeout + 'ms.');
    }
  }
  if (thisStep > 0 && thisStep != lastStep) {
    stepStartTime = Date.now() - startTime;
    console.log('Step ' + thisStep + ': ' + thisMsg + ' STARTED at ' + stepStartTime + 'ms.');
    prevMsg = thisMsg;
    prevStep = thisStep;
  }
};

function isAlertPresent() {
  try {
    var thisAlert = $browser.switchTo().alert();
    return true;
  } catch (err) { return false; }
}

function isElementSelected(el) { return $browser.findElement(el).isSelected(); }

function isTextPresentIn(text, sourceEl) {
  return sourceEl.getText()
  .then(function (wholetext) {
    return wholetext.indexOf(text) != -1;
  });
}

function isTextPresent(text) {
  return isTextPresentIn(text, $browser.findElement(By.tagName('html')));
}

/** BEGINNING OF SCRIPT **/

// Setting User Agent is not then-able, so we do this first (if defined and not default)
if (UserAgent && (0 !== UserAgent.trim().length) && (UserAgent != 'default')) {
  $browser.addHeader('User-Agent', UserAgent);
  console.log('Setting User-Agent to ' + UserAgent);
}

$browser.manage().deleteAllCookies();

// Get browser capabilities and do nothing with it, so that we start with a then-able command
$browser.getCapabilities().then(function () { })

// Step 1
.then(function() {
  log(1, '$browser.get("https://login.siplay.com/Users/Login?leagueId=12866202")');
  return $browser.get("https://login.siplay.com/Users/Login?leagueId=12866202"); })

// Step 2
.then(function() {
  log(2, 'setElementText "Email"');
  return $browser.waitForAndFindElement(By.id("Email"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("test.account@test.sip.com"); })

// Step 3
.then(function() {
  log(3, 'setElementText "Password"');
  return $browser.waitForAndFindElement(By.id("Password"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("Password01"); })

// Step 4
.then(function() {
  log(4, 'clickElement "//form[@id=\'loginForm\']//button[.=\' Login\']"');
  return $browser.waitForAndFindElement(By.xpath("//form[@id=\'loginForm\']//button[.=\' Login\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 5
.then(function() {
  log(5, 'clickElement "//ul[@class=\'account-list\']//button[.=\'Login\']"');
  return $browser.waitForAndFindElement(By.xpath("//ul[@class=\'account-list\']//button[.=\'Login\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 6
.then(function() {
  log(6, 'clickElement "Register to Play/Participate"');
  return $browser.waitForAndFindElement(By.linkText("Register to Play/Participate"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 7
.then(function() {
  log(7, 'setElementSelected "Children_10020429"');
  return $browser.waitForAndFindElement(By.id("Children_10020429"), DefaultTimeout); })
.then(function(el) { el.isSelected()
  .then(function(bool) { if (!bool) { el.click(); } }); })

// Step 8
.then(function() {
  log(8, 'clickElement "button.btn.btn-primary"');
  return $browser.waitForAndFindElement(By.css("button.btn.btn-primary"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 9
.then(function() {
  log(9, 'clickElement "button.btn.btn-primary"');
  return $browser.waitForAndFindElement(By.css("button.btn.btn-primary"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 10
.then(function() {
  log(10, 'clickElement "button.btn.btn-primary"');
  return $browser.waitForAndFindElement(By.css("button.btn.btn-primary"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 11
.then(function() {
  log(11, 'clickElement "Continue to Checkout"');
  return $browser.waitForAndFindElement(By.linkText("Continue to Checkout"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 12
.then(function() {
  log(12, 'clickElement "Pay with Check"');
  return $browser.waitForAndFindElement(By.linkText("Pay with Check"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 13
.then(function() {
  log(13, 'clickElement "Logout"');
  return $browser.waitForAndFindElement(By.linkText("Logout"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 13.5
.then(function() {
	return $browser.switchTo().alert().accept(); })

// Step 14
.then(function() {
  log(14, '$browser.get("https://login.siplay.com/Users/Login?leagueId=12866202")');
  return $browser.get("https://login.siplay.com/Users/Login?leagueId=12866202"); })

// Step 15
.then(function() {
  log(15, 'setElementText "Email"');
  return $browser.waitForAndFindElement(By.id("Email"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("cleanup.account@sip.test.com"); })

// Step 16
.then(function() {
  log(16, 'sendKeysToElement "Password"');
  return $browser.waitForAndFindElement(By.id("Password"), DefaultTimeout); })
.then(function (el) { el.sendKeys("\\9"); })

// Step 17
.then(function() {
  log(17, 'setElementText "Password"');
  return $browser.waitForAndFindElement(By.id("Password"), DefaultTimeout); })
.then(function (el) {
  el.clear();
  el.sendKeys("AutomatedTestPassword01"); })

// Step 18
.then(function() {
  log(18, 'clickElement "//form[@id=\'loginForm\']//button[.=\' Login\']"');
  return $browser.waitForAndFindElement(By.xpath("//form[@id=\'loginForm\']//button[.=\' Login\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 19
.then(function() {
  log(19, 'clickElement "//ul[@class=\'account-list\']//button[.=\'Login\']"');
  return $browser.waitForAndFindElement(By.xpath("//ul[@class=\'account-list\']//button[.=\'Login\']"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 20
.then(function() {
  log(20, 'clickElement "Registrations"');
  return $browser.waitForAndFindElement(By.linkText("Registrations"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 21
.then(function() {
  log(21, 'clickElement "http___devprocessmonitor_siplay_com_lss_admin_registrationdetail_aspx"');
  return $browser.waitForAndFindElement(By.id("http___devprocessmonitor_siplay_com_lss_admin_registrationdetail_aspx"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 22
.then(function() {
  log(22, 'clickElement "btnView"');
  return $browser.waitForAndFindElement(By.id("btnView"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 23
.then(function() {
  log(23, 'clickElement "Edit Acct"');
  return $browser.waitForAndFindElement(By.linkText("Edit Acct"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 24
.then(function() {
  log(24, 'clickElement "Order History"');
  return $browser.waitForAndFindElement(By.linkText("Order History"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 25
.then(function() {
  log(25, 'clickElement "Edit"');
  return $browser.waitForAndFindElement(By.linkText("Edit"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 26
.then(function() {
  log(26, 'clickElement "Unregister"');
  return $browser.waitForAndFindElement(By.linkText("Unregister"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 26.5
.then(function() {
	return $browser.switchTo().alert().accept(); })

// Step 27
.then(function() {
  log(27, 'clickElement "btnDone"');
  return $browser.waitForAndFindElement(By.id("btnDone"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 28
.then(function() {
  log(28, 'clickElement "Done"');
  return $browser.waitForAndFindElement(By.linkText("Done"), DefaultTimeout); })
.then(function (el) { el.click(); })

// Step 29
.then(function() {
  log(29, 'clickElement "Log Out"');
  return $browser.waitForAndFindElement(By.linkText("Log Out"), DefaultTimeout); })
.then(function (el) { el.click(); })

.then(function() {
  log(lastStep, '');
  console.log('Browser script execution SUCCEEDED.');
}, function(err) {
  console.log ('Browser script execution FAILED.');
  throw(err);
});

/** END OF SCRIPT **/
