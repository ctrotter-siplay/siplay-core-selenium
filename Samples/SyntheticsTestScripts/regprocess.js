var wd = require('wd')
  , _ = require('underscore')
  , fs = require('fs')
  , path = require('path')
  , uuid = require('uuid-js');
var VARS = {};

var b = wd.promiseRemote();

b.on('status', function(info){console.log('[36m%s[0m', info);});b.on('command', function(meth, path, data){  console.log(' > [33m%s[0m: %s', meth, path, data || '');});
b.init({
  browserName:'firefox'
})
.then(function () { return b.get("https://login.siplay.com/Users/CreateUser?associatedLeague=12866202"); })
.then(function () { return b.elementById("Email"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "zlddjirp@6paq.com"); });
})
.then(function () { return b.elementById("ConfirmEmail"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "zlddjirp@6paq.com"); });
})
.then(function () { return b.elementById("Password"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "Password01"); });
})
.then(function () { return b.elementById("ConfirmPassword"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "Password01"); });
})
.then(function () { return b.elementByXPath("//div[@class='panel-body']//button[.='Create User']"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementById("FirstName"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "FirstName"); });
})
.then(function () { return b.elementById("LastName"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "LastName"); });
})
.then(function () { return b.elementById("Street"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "385462 Union St"); });
})
.then(function () { return b.elementById("City"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "Palo Alto"); });
})
.then(function () { return b.elementById("State"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "CA"); });
})
.then(function () { return b.elementById("Zip"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "90210"); });
})
.then(function () { return b.elementById("Phone"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "345-237-4444"); });
})
.then(function () { return b.elementByXPath("//div[@class='panel-footer']//button[.=' Create Account']"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementByLinkText("Skip"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementByXPath("//label[@for='New_Children']"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementById("New_Children"); })
.then(function (el) { return b.isSelected(el)
  .then(function (isSelected) {
     if (!isSelected) {
       return b.clickElement(el);
     }
  });
})
.then(function () { return b.elementByCssSelector("button.btn.btn-primary"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementById("FirstName"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "Child1"); });
})
.then(function () { return b.elementById("LastName"); })
.then(function (el) { return b.type(el, "\\9"); })
.then(function () { return b.elementById("Nickname"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "Mittens"); });
})
.then(function () { return b.elementById("Birthdate"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "01/01/2008"); });
})
.then(function () { return b.elementByXPath("//select[@id='Grade']//option[1]"); })
.then(function (el) { return b.isSelected(el)
  .then(function (isSelected) {
     if (!isSelected) {
       return b.clickElement(el);
     }
  });
})
.then(function () { return b.elementByXPath("//select[@id='Grade']//option[1]"); })
.then(function (el) { return b.isSelected(el)
  .then(function (isSelected) {
     if (!isSelected) {
       return b.clickElement(el);
     }
  });
})
.then(function () { return b.elementByXPath("//select[@id='Grade']//option[9]"); })
.then(function (el) { return b.isSelected(el)
  .then(function (isSelected) {
     if (!isSelected) {
       return b.clickElement(el);
     }
  });
})
.then(function () { return b.elementByXPath("//select[@id='Grade']//option[9]"); })
.then(function (el) { return b.isSelected(el)
  .then(function (isSelected) {
     if (!isSelected) {
       return b.clickElement(el);
     }
  });
})
.then(function () { return b.elementByXPath("//label[@for='GenderValue_0']"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementById("GenderValue_0"); })
.then(function (el) { return b.isSelected(el)
  .then(function (isSelected) {
     if (!isSelected) {
       return b.clickElement(el);
     }
  });
})
.then(function () { return b.elementById("MedicalInfoValue"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "None"); });
})
.then(function () { return b.elementByXPath("//div[@id='contentArea']//button[.='Save']"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementByCssSelector("button.btn.btn-primary"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementByCssSelector("button.btn.btn-primary"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementById("FirstName"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "Spouse"); });
})
.then(function () { return b.elementById("LastName"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "LastName"); });
})
.then(function () { return b.elementById("Phone"); })
.then(function (el) { return b.clear(el)
  .then(function () { return b.type(el, "552-333-6666"); });
})
.then(function () { return b.elementByXPath("//div[@id='contentArea']//button[.='Save & Continue']"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementByLinkText("Continue to Checkout"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementByLinkText("Pay with Check"); })
.then(function (el) { return b.clickElement(el); })
.then(function () { return b.elementByLinkText("Logout"); })
.then(function (el) { return b.clickElement(el); })
.fin(function () {
b.quit();
}).done();
