import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.io.File;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.*;
import static org.openqa.selenium.OutputType.*;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class smoketest-production {
    public static void main(String[] args) throws Exception {
        RemoteWebDriver wd;
        DesiredCapabilities caps = DesiredCapabilities.firefox();
            caps.setCapability("name", "smoketest-production");
        wd = new RemoteWebDriver(
            new URL("http://ctrotter:6a1c7676-5aed-4a4a-a906-ab335f390218@ondemand.saucelabs.com:80/wd/hub"),
            caps);
        wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        wd.get("https://login.siplay.com/Users/Login?leagueId=12866202");
        wd.findElement(By.id("Email")).click();
        wd.findElement(By.id("Email")).clear();
        wd.findElement(By.id("Email")).sendKeys("cleanup.account@sip.test.com");
        wd.findElement(By.id("Password")).click();
        wd.findElement(By.id("Password")).clear();
        wd.findElement(By.id("Password")).sendKeys("AutomatedTestPassword01");
        wd.findElement(By.xpath("//form[@id='loginForm']//button[.=' Login']")).click();
        wd.findElement(By.linkText("Website")).click();
        wd.findElement(By.id("_admin_website_menu")).click();
        wd.findElement(By.xpath("//aside[@id='left-panel']/nav/ul/li[7]/a/span")).click();
        wd.findElement(By.id("_admin_scheduling_view_schedules")).click();
        wd.findElement(By.cssSelector("input.btn.btn-success")).click();
        wd.findElement(By.linkText("Registrations")).click();
        wd.findElement(By.id("http___devprocessmonitor_siplay_com_lss_admin_registrationdetail_aspx")).click();
        wd.findElement(By.linkText("View Report")).click();
        wd.findElement(By.linkText("Accounts")).click();
        wd.findElement(By.id("http___devprocessmonitor_siplay_com_lss_admin_reports_allaccounts_aspx")).click();
        wd.findElement(By.linkText("Teams")).click();
        wd.findElement(By.id("http___devprocessmonitor_siplay_com_admin_teams")).click();
        wd.findElement(By.linkText("Setup")).click();
        wd.findElement(By.linkText("Organization Settings")).click();
        wd.findElement(By.id("http___devprocessmonitor_siplay_com_admin_agreements_manageserviceagreement")).click();
        wd.findElement(By.id("http___devprocessmonitor_siplay_com_lss_admin_dashboard_aspx")).click();
        wd.findElement(By.linkText("Log Out")).click();
        wd.quit();
    }
    
    public static boolean isAlertPresent(FirefoxDriver wd) {
        try {
            wd.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }
}
